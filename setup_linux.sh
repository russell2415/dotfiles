#!/bin/bash

set -e

NOW=$(date +"%Y%m%d-%H%M")
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

DOTFILES="vimrc gitconfig screenrc tkconrc abcde.conf"

# Create backup directory for original dotfiles
if [ -e "$HOME/.bak" ]
then
  echo "Backup directory already exists"
else
  echo "Creating backup directory ~/.bak"
  mkdir "$HOME/.bak"
fi

# Backup original dotfiles and symlink to yours
for file in $DOTFILES; do
    if [ -e "$HOME/.$file" ]
    then
        echo "Backing up original .$file";
        mv "$HOME/.$file" "$HOME/.bak/.$file-$NOW"
    fi

    ln -sf $DIR/conf/$file ~/.$file
done

mkdir -p "$HOME/.vim/autoload"
mkdir -p "$HOME/.vim/bundle"

vim-addon-manager install colors-sampler-pack
vim-addon-manager install ultisnips
vim-addon-manager install youcompleteme

# Verify Required Packages Installed
PACKAGES="vim-addon-manager vim-ultisnips vim-youcompleteme"
for file in $PACKAGES; do
    if [[ ! $(dpkg -s vim-addon-manager) ]]; then
        echo "Error: Required package not installed: $PACKAGE"
    fi
done

# Link to submodules old-style Vim plugins
mkdir -p ~/.vim/plugin
if [[ ! -L ~/.vim/plugin/ScrollColors ]]; then
    echo "Adding symlink to ScrollColors"
    ln -sf $DIR/vim_modules/ScrollColors ~/.vim/plugin/ScrollColors
fi
if [[ ! -L ~/.vim/plugin/comments.vim ]]; then
    echo "Adding symlink to comments.vim"
    ln -sf $DIR/vim_modules/comments.vim ~/.vim/plugin/comments.vim
fi

# Link to fork of Ultisnips
if [[ ! -L ~/.vim/UltiSnips ]]; then
    echo "Adding symlink to UltiSnips"
    ln -sf $DIR/vim_modules/vim-snippets/UltiSnips ~/.vim/UltiSnips
fi
